A method to grab the comments from YouTube videos

* Requires qwarc and realpath from GNU coreutils 8.23+.
* Execute as `comments VIDEOID` where `VIDEOID` is the 11-character video ID from YouTube.
* You can pass multiple video IDs at once as well: `comments VIDEOID1 VIDEOID2 ...`. They get executed sequentially.
* Comments are grabbed only in the 'newest first' order, including replies and nesting.
* Everything's written to a few files in the current directory called `youtube-comments-VIDEOID-DATE*`.
 * After the retrieval finished cleanly and you're satisfied with the results, you can delete the `.db` and `.log` files. The former is essentially useless, and the latter is contained in the `-meta.warc.gz` file.
